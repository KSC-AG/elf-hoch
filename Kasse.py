class Kasse:
    def __init__(self):
        self.__stand = 0

    def auszahlen(self, pBetrag):
        self.__stand -= pBetrag

    def einzahlen(self, pBetrag):
        self.__stand += pBetrag

    def getStand(self):
        return self.__stand
