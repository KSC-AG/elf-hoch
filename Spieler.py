from Kasse import *

class Spieler:
    def __init__(self, pName, pWuerfel1, pWuerfel2, pKasse):
        self.__name = pName
        self.__marken = 50
        self.rwuerfel1 = pWuerfel1
        self.rwuerfel2 = pWuerfel2
        self.rkasse = pKasse

    def spielen(self):
        self.rwuerfel1.werfen()
        self.rwuerfel2.werfen()
        zahl1 = self.rwuerfel1.getAugenzahl()
        zahl2 = self.rwuerfel2.getAugenzahl()
        summe = zahl1 + zahl2
        differenz = abs(11 - summe)  # Zahl ist immer positiv
        if summe == 11:
            self.__marken += self.rkasse.getStand()
            self.rkasse.auszahlen(self.rkasse.getStand())
        elif summe < 11:
            self.rkasse.einzahlen(differenz)
            self.__marken -= differenz
        elif summe > 11:
            self.rkasse.einzahlen(summe)
            self.__marken -= summe

        print(f"{self.__name} hat {summe} gewürfelt und hat {self.__marken} Marken")

    def getName(self):
        return self.__name

    def getMarken(self):
        return self.__marken
