from Wuerfel import *
from Spieler import *
from Kasse import *

class Spielmanager:
    def __init__(self, name, liste):
        self.name = name
        self.rSpieler = liste

    def spielrundeDurchfuehren(self):
        runden = 0
        while True:  # einfache Enlosschleife
            runden += 1
            print(f"\nRunde {runden}:")
            for spieler in self.rSpieler:
                spieler.spielen()
                # Wenn der Spieler, der gerade von der Schleife behandelt wird, über 150 hat, wird die Schleife beendet
                if spieler.getMarken() >= 150:  # (Und der Gewinner geprinted natürlich)
                    print(f"\nDer Gewinner ist {spieler.getName()} mit {spieler.getMarken()} Marken")
                    return
            print(f"Kasse: {self.rSpieler[0].rkasse.getStand()} Marken")

# ======================================= Testen der Klassen =======================================
kasse = Kasse()
wuerfel1 = Wuerfel()
wuerfel2 = Wuerfel()
spieler1 = Spieler("Kilian", wuerfel1, wuerfel2, kasse)
spieler2 = Spieler("Jonas", wuerfel1, wuerfel2, kasse)
spieler3 = Spieler("Philipp", wuerfel1, wuerfel2, kasse)
spielerliste = [spieler1, spieler2, spieler3]
spielmanager = Spielmanager("MANAGER", spielerliste)
spielmanager.spielrundeDurchfuehren()
