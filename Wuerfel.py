from random import randint


class Wuerfel:
    def __init__(self):
        self.__augen = 0

    def werfen(self):
        self.__augen = randint(1, 6)
        return self.__augen

    def getAugenzahl(self):
        return self.__augen

